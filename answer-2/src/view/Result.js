import React from 'react';
// import ReactDom from 'react-dom'
import {connect} from 'react-redux'
import { Button } from 'antd-mobile';

class Result extends React.Component{
   
    render(){
    
      console.log(this.props);
      return(
        <div>
            <h1>恭喜你获得{this.props.location.state.score}分</h1>
          <Button onClick={this.goDatiPage}> 回到首页 </Button>
          
        </div>
      )
  
    }
    goDatiPage=() => {
      console.log(this.props);
      this.props.history.push('/')
    }


  }
  
  const addAction = {
    type: 'add'
  }
  
  
  
  //将状态映射到props函数
  function mapStateToProps(state){
    return{
      value: state.num
    }
  }
  //将修改state数据的方法，映射到props,，默认会传入dispach方法
  function mapDispatchToProps(dispatch){
    return {
      onAddClick:()=>{
        dispatch(addAction)
      },
      onAddClick5:()=>{
        //动作类型
      dispatch({type:'addNum',num:5})
      }
    }
  }
  //将上面的这两个方法，将数据仓库的state和修改state的方法映射到组件上，形成新的组件
  const App = connect(
    mapStateToProps,
    mapDispatchToProps
  )(Result)

  export default App
  