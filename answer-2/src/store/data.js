import {createStore} from 'redux'
import methods from './methods'
import state from './state'

//初始化状态创建reduuer函数
let data = state;

//初创建reducer函数
    let ActionFnobj= methods
   
  function reducer (state = data,action){
    // console.log(state);
    // console.log(action);
    if (action.type.indexOf('redux') === -1) {
      state =ActionFnobj[action.type](state,action)
      return {...state}
    }else{
      return state
    }
    
   
  }
  const store = createStore(reducer)

  export default store